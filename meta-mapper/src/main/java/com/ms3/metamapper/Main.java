package com.ms3.metamapper;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.ms3.metamapper.core.JSONEtractor;
import com.ms3.metamapper.core.MetaExtractor;
import com.ms3.metamapper.core.XMLExtractor;
import com.ms3.metamapper.entity.Instruction;

/**
 * Oct 6, 2015 10:16:22 PM
 *
 * @author © tdelacerna <tdelacerna@ms3-inc.com>
 */

public class Main {
	
	static MetaExtractor<JSONObject> jsonExtractor = new JSONEtractor();
	static MetaExtractor<Map> xmlExtractor = new XMLExtractor();
    
    private static final String metaStyle = "C:\\DEV\\Resources\\Docs\\mapping-stylesheet.json";
    private static final String input = "C:\\DEV\\Resources\\Docs\\input.json";
    
    public static void constructOutput( Map<String,Object> metaStyle, Map mapInput ){
    	Map baseInfo = (Map)metaStyle.get("baseInfo");
    	String inputType = (String)baseInfo.get("inputType");
    	String outputType = (String)baseInfo.get("outputType");
    	String baseNode = (String)baseInfo.get("baseNode");
    	String encoding = (String)baseInfo.get("encoding");
    	
    	List<Instruction> instructions = new ArrayList();
    	for( String key : metaStyle.keySet() ){
    		if( key.contains("instruction")){
    			System.out.println("=============");
    			Map<String,Object> instr = (Map)metaStyle.get(key);
    			Instruction instruction = null;
    			String inputField = null;
    			String outputField = null;
    			Map map = null;
    			for( String k : instr.keySet() ){
    				inputField = (String)instr.get("inputField");
    				outputField = (String)instr.get("outputField");
    				map = (Map)instr.get("arrList");
    			}
    			instruction = new Instruction( key, inputField, outputField, map );
    			instructions.add( instruction );
    		}
    	}
    	
    	StringBuilder sb = new StringBuilder();
    	sb.append("<").append(baseNode).append(">\n");
    	System.out.println( "~~~~~~~~~~~> " + instructions.size() );
    	for( Instruction instr : instructions ){
    		String infield = instr.getInputField();
    		String outfield = instr.getOutputField();
    		Map map = instr.getMap();
    		
    		int startIdxOut = outfield.indexOf(":/");
    		int startIdxIn = infield.indexOf(":/");
    		
    		String fInfield = infield.substring(startIdxIn+2);
    		String fOutfield = outfield.substring(startIdxOut+2);
    		String inVal = (String)mapInput.get( fInfield );
    		
    		String str[] = fOutfield.split("/");
    		
    		sb.append("\t<").append(fOutfield).append(">")
    		  .append(inVal)
    		  .append("</").append(fOutfield).append(">\n");
    		
    	}	
    	sb.append("</").append(baseNode).append(">");
    	
    	System.out.println( sb.toString() );
    	
    }
    
    public static void main(String[] args) {
        try {
            JSONParser parser = new JSONParser();
            
            FileReader frMeta = new FileReader(metaStyle);
            FileReader frInput = new FileReader(input);
            
            Object objMeta = parser.parse( frMeta );
            Object objInput = parser.parse( frInput );
            
            JSONObject jsonMeta = (JSONObject) objMeta;
            JSONObject jsonInput = (JSONObject) objInput;
            
            Map<String,Object> mapMeta = jsonExtractor.extractType(jsonMeta);
            Map<String,Object> mapInput = jsonExtractor.extractType(jsonInput);
            
            constructOutput( mapMeta, mapInput );
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    
	
}

