package com.ms3.metamapper.core;

import java.util.Map;

/**
 * Oct 6, 2015 10:52:17 PM
 *
 * @author © tdelacerna <tdelacerna@ms3-inc.com>
 */

public interface MetaExtractor<T> {
	
	public Map extractType( T t );
	
}

