package com.ms3.metamapper.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Oct 6, 2015 11:17:09 PM
 *
 * @author © tdelacerna <tdelacerna@ms3-inc.com>
 */

public class JSONEtractor implements MetaExtractor<JSONObject>{
	
	public Map extractType(JSONObject t) {
		Iterator<String> keys = t.keySet().iterator();
        Map<String,Object> map = new HashMap<String,Object>();
        while( keys.hasNext() ){
            String key = keys.next();
            Object value = t.get(key);
            
            if( value instanceof JSONObject ){
            	Map m = extractType( (JSONObject)value );
                map.put(key, m);
            }else if( value instanceof JSONArray ){
                JSONArray arr = (JSONArray)value;
                Iterator i = arr.iterator();
                while( i.hasNext() ){
                    Object o = i.next();
                    if( o instanceof JSONObject ){
                    	Map m = extractType((JSONObject)o);
                        map.put(key,m);
                    }
                }
            }else{
            	map.put(key,value);
            }
        }
        
        return map;
	}
	
}

