package com.ms3.metamapper.entity;

import java.util.Map;

/**
 * Oct 6, 2015 11:30:04 PM
 *
 * @author © tdelacerna <tdelacerna@ms3-inc.com>
 */

public class Instruction{
	
	String name = null;
	String inputField = null;
	String outputField = null;
	Map map = null;
	
	public Instruction(){}
	
	public Instruction( String name, String inputField, String outputField, Map map ){
		this.name = name;
		this.inputField = inputField;
		this.outputField = outputField;
		this.map = map;
	}
	
	public void setName( String name ){
		this.name=name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getInputField() {
		return inputField;
	}

	public void setInputField(String inputField) {
		this.inputField = inputField;
	}

	public String getOutputField() {
		return outputField;
	}

	public void setOutputField(String outputField) {
		this.outputField = outputField;
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}
	
}

